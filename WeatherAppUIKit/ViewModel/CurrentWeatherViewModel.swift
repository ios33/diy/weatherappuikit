//
//  CurrentWeatherViewModel.swift
//  WeatherAppUIKit
//
//  Created by HongDT on 11/28/20.
//

import Foundation
import Combine

class CurrentWeatherViewModel {
    var weathers: [CurrentWeather] = []
    
    func fetchData(completionHandler: @escaping () -> Void) {
        var ids = ""
        
        for city in AppData.cityList.cities {
            ids += "\(city),"
        }
        
        guard let url = URL(
                string: "http://api.openweathermap.org/data/2.5/group?id=\(ids)&appid=\(AppData.apiKey)")
        else {
            print("Invalid URL")
            completionHandler()
            return
        }
        
        let request = URLRequest(url: url)
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            if let data = data,
               let weather = try? JSONDecoder().decode(
                CurrentWeatherList.self,
                from: data
               ) {
                DispatchQueue.main.async { [weak self] in
                    if let list = weather.list {
                        self?.weathers = list
                        completionHandler()
                    }
                }
                return
            }
            print("Fetch failed: \(error?.localizedDescription ?? "Unknown error")")
            completionHandler()
        }.resume()
    }
}


