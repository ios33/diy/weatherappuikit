//
//  CurrentWeather.swift
//  WeatherAppUIKit
//
//  Created by HongDT on 11/28/20.
//

import UIKit

struct CurrentWeatherList: Hashable, Codable {
    var list: [CurrentWeather]?
}

struct CurrentWeather: Hashable, Codable {
    var cod: Int?
    var cityID: Int64?
    var cityName: String?
    var main: Main?
    var weathers: [Weather]?
    
    struct Main: Hashable, Codable {
        var temp: Float
        var humidity: Float
    }
    
    struct Weather: Hashable, Codable {
        var description: String
    }
    
    enum CodingKeys: String, CodingKey {
        case cod
        case cityID = "id"
        case cityName = "name"
        case main
        case weathers = "weather"
    }
}
