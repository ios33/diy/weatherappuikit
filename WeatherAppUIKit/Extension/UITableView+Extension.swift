//
//  UITableView+Extension.swift
//  WeatherAppUIKit
//
//  Created by HongDT on 11/27/20.
//

import UIKit.UITableView
import ESPullToRefresh

extension UITableView {
    func setupRefreshControl(refreshControl: UIRefreshControl) {
        // Add Refresh Control to Table View
        if #available(iOS 10.0, *) {
            self.refreshControl = refreshControl
        } else {
            self.addSubview(refreshControl)
        }
    }
    
    func registerCellNib<T: UITableViewCell>(_ type: T.Type) {
        register(T.nib(), forCellReuseIdentifier: T.name)
    }
    
    func registerCellClass<T: UITableViewCell>(_ type: T.Type) {
        register(T.self, forCellReuseIdentifier: T.name)
    }
    
    func registerHeaderFooterNib<T: UIView>(for type: T.Type) {
        register(T.nib(), forHeaderFooterViewReuseIdentifier: T.name)
    }
    
    func registerHeaderFooterClass<T: UIView>(for type: T.Type) {
        register(T.self, forHeaderFooterViewReuseIdentifier: T.name)
    }
    
    func dequeueReusableCell<T: UITableViewCell>(_ type: T.Type) -> T {
        return dequeueReusableCell(withIdentifier: T.name) as! T
    }
    
    static func pullToRefreshHeader() -> ESRefreshHeaderAnimator{
        let h = ESRefreshHeaderAnimator.init(frame: CGRect.zero)
        h.pullToRefreshDescription = "Pull to refresh"
        h.releaseToRefreshDescription = "Release to refresh"
        h.loadingDescription = "Refreshing..."
        return h
    }
    
    static func pullToRefreshFooter() -> ESRefreshFooterAnimator {
        let f = ESRefreshFooterAnimator.init(frame: CGRect.zero)
        f.loadingMoreDescription = "Load more"
        f.noMoreDataDescription = ""
        f.loadingDescription = "Loading..."
        return f
    }
}
