//
//  UIView+Extension.swift
//  WeatherAppUIKit
//
//  Created by HongDT on 11/27/20.
//

import UIKit.UIView

extension UIView {
    
    /// Nib
    ///
    /// - Returns: the Nib (xib) of this view if it exits
    @objc class func nib() -> UINib {
        return UINib(nibName: name, bundle: nil)
    }
    
    class func loadFromNib<T: UIView>() -> T {
        return Bundle.main.loadNibNamed( name, owner: nil, options: nil)![0] as! T
    }
    
}
    
