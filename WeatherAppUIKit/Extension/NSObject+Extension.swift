//
//  NSObject+Extension.swift
//  WeatherAppUIKit
//
//  Created by HongDT on 11/27/20.
//

import Foundation

extension NSObject {
    
    @objc public class var name: String {
        get {
            return String(describing: self)
        }
    }
    
}
