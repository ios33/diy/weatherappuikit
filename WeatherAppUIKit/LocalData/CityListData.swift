//
//  CityListData.swift
//  WeatherAppUIKit
//
//  Created by HongDT on 11/28/20.
//

import Foundation

final class CityListData {
    static let citiesKey = "Cities"
    
    static func loadCities() -> [Int64] {
        let savedCities = UserDefaults.standard.object(forKey: CityListData.citiesKey)
        if let savedCities = savedCities as? Data {
          let decoder = JSONDecoder()
          return (try? decoder.decode([Int64].self, from: savedCities))
            ?? []
        }
        return []
    }
    
    var cities = loadCities() {
      didSet {
        persistCities()
      }
    }

    func addCity(city: Int64) {
      let newCity = city
      cities.append(newCity)
    }

    func deleteCity(at offsets: Int) {
        cities.remove(at: offsets)
    }

    private func persistCities() {
      let encoder = JSONEncoder()
      if let encoded = try? encoder.encode(cities) {
        UserDefaults.standard.set(encoded, forKey: CityListData.citiesKey)
      }
    }
}
