//
//  DetailWeatherVC.swift
//  WeatherAppUIKit
//
//  Created by HongDT on 11/27/20.
//

import UIKit

class DetailWeatherVC: UIViewController {

    @IBOutlet weak var cityNameLbl: UILabel!
    @IBOutlet weak var tempLbl: UILabel!
    @IBOutlet weak var humidityLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    
    var weather: CurrentWeather!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setContent()
    }
    
    func setContent() {
        cityNameLbl.text = weather.cityName
        tempLbl.text = "Temp : \(String(format: "%.2f °C", (weather.main?.temp ?? 0) - 273.15))"
        humidityLbl.text = "Humidity : \(String(format: "%.2f", (weather.main?.humidity ?? 0)))"
        descriptionLbl.text = "Description : \(weather.weathers?.first?.description ?? "N/A")"
    }
}
