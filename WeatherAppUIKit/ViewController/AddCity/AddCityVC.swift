//
//  AddCityVC.swift
//  WeatherAppUIKit
//
//  Created by HongDT on 11/28/20.
//

import UIKit

class AddCityVC: UIViewController {
    
    @IBOutlet weak var cityNameTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let cancelItem = UIBarButtonItem(
            barButtonSystemItem: .cancel,
            target: self,
            action: #selector(pop)
        )
        let addItem = UIBarButtonItem(
            title: "Add",
            style: .plain,
            target: self,
            action: #selector(addNewCity)
        )
        
        navigationItem.backBarButtonItem = nil
        navigationItem.leftBarButtonItem = cancelItem
        navigationItem.rightBarButtonItem = addItem
    }
    
    
    @objc private func pop() {
        navigationController?.popViewController(animated: true)
    }
    
    @objc private func addNewCity() {
        guard var name = cityNameTextField.text else {
            self.showErrorAlertView(message: "Something went wrong.")
            return
        }
        
        name = name.isEmpty ? "Hanoi" : name
        
        guard let url = URL(string: "http://api.openweathermap.org/data/2.5/weather?q=\(name)&appid=\(AppData.apiKey)") else {
            print("Invalid URL")
            self.showErrorAlertView(message: "Something went wrong.")
            return
        }
        
        let request = URLRequest(url: url)
        
        URLSession.shared.dataTask(with: request) { [weak self] data, response, error in
            if let data = data {
                if let weather = try? JSONDecoder().decode(CurrentWeather.self, from: data) {
                    if let cod = weather.cod, cod == 200, let id = weather.cityID {
                        if (!AppData.cityList.cities.contains(id)) {
                            AppData.cityList.addCity(city: id)
                            DispatchQueue.main.async {
                                self?.navigationController?.popViewController(animated: true)
                            }
                        } else {
                            self?.showErrorAlertView(message: "City have existed. Try another city.")
                        }
                    } else {
                        self?.showErrorAlertView(message: "City not found. Please try again.")
                    }
                    
                    return
                }
            }
            print("Fetch failed: \(error?.localizedDescription ?? "Unknown error")")
            self?.showErrorAlertView(message: "Something went wrong.")
        }.resume()
    }
    
    private func showErrorAlertView(message: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(
                title: "Error",
                message: message,
                preferredStyle: .alert
            )
            alert.addAction(UIAlertAction(
                title: "Cancel",
                style: .cancel,
                handler: nil
            ))
            self.present(alert, animated: true, completion: nil)
        }
    }
}

