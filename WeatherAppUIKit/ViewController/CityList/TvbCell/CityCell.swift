//
//  CityCell.swift
//  WeatherAppUIKit
//
//  Created by HongDT on 11/27/20.
//

import UIKit

class CityCell: UITableViewCell {

    @IBOutlet weak var cityNameLbl: UILabel!
    @IBOutlet weak var tempLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setContent(weather: CurrentWeather) {
        cityNameLbl.text = weather.cityName
        tempLbl.text = "\(String(format: "%.2f °C ", (weather.main?.temp ?? 0) - 273.15))"
    }
    
}
