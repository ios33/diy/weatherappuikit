//
//  CityListVC.swift
//  WeatherAppUIKit
//
//  Created by HongDT on 11/27/20.
//

import UIKit

class CityListVC: UIViewController {

    @IBOutlet weak var cityTbv: UITableView!
    
    let allItems = ["Hanoi", "Haiphong"]
    var currentWeatherViewModel = CurrentWeatherViewModel()
    
    private let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        currentWeatherViewModel.fetchData(completionHandler: { [weak self] in
            self?.cityTbv.reloadData()
        })
        
        setupNavigationBar()
        setupTableView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        currentWeatherViewModel.fetchData(completionHandler: { [weak self] in
            self?.cityTbv.reloadData()
        })
    }
    
    private func setupNavigationBar() {
        title = "Weather in your city"
        let addItem = UIBarButtonItem(
            barButtonSystemItem: UIBarButtonItem.SystemItem.add,
            target: self,
            action: #selector(addNewCity)
        )
        navigationItem.rightBarButtonItem = addItem
    }
    
    private func setupTableView() {
        cityTbv.register(CityCell.nib(), forCellReuseIdentifier: CityCell.name)
        
        cityTbv.delegate = self
        cityTbv.dataSource = self
        cityTbv.tableFooterView = UIView()
        
        cityTbv.setupRefreshControl(refreshControl: refreshControl)
        refreshControl.addTarget(
            self,
            action: #selector(pullToRefresh),
            for: .valueChanged
        )
        
        let longPressGesture = UILongPressGestureRecognizer(
            target: self,
            action: #selector(handleLongPress)
        )
        longPressGesture.minimumPressDuration = 0.5
        cityTbv.addGestureRecognizer(longPressGesture)
    }
    
    @objc func addNewCity() {
        self.navigationController?.pushViewController(AddCityVC(), animated: true)
    }
    
    @objc func pullToRefresh() {
        currentWeatherViewModel.fetchData(completionHandler: { [weak self] in
            self?.cityTbv.reloadData()
            self?.refreshControl.endRefreshing()
        })
    }
    
    @objc func handleLongPress(longPressGesture: UILongPressGestureRecognizer) {
        let p = longPressGesture.location(in: self.cityTbv)
        let indexPath = self.cityTbv.indexPathForRow(at: p)
        guard let indexPath = indexPath else {
            return
        }
        if longPressGesture.state == UIGestureRecognizer.State.began {
            let alert = UIAlertController(
                title: "Are you sure you want to delete this?",
                message: "There is no undo.",
                preferredStyle: .alert
            )
            alert.addAction(UIAlertAction(
                title: "Cancel",
                style: .cancel
            ))
            alert.addAction(UIAlertAction(
                title: "Delete",
                style: .destructive,
                handler: { [weak self] _ in
                    AppData.cityList.deleteCity(at: indexPath.row)
                    self?.currentWeatherViewModel.fetchData() {
                        self?.cityTbv.reloadData()
                    }
                }
            ))
            self.present(alert, animated: true)
        }
    }
}

extension CityListVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentWeatherViewModel.weathers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(
            withIdentifier: CityCell.name,
            for: indexPath
        ) as! CityCell
        cell.setContent(weather: currentWeatherViewModel.weathers[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let detailWeatherVC = DetailWeatherVC()
        detailWeatherVC.weather = currentWeatherViewModel.weathers[indexPath.row]
        self.navigationController?.pushViewController(detailWeatherVC, animated: true)
    }
}
